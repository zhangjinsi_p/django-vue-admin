from django.apps import AppConfig


class PermissionConfig(AppConfig):
    name = 'system'
    verbose_name = "权限管理"
