---
home: true
heroImage: /logo.png
heroText: Django-Vue-Admin
tagline: 使用django+vue进行极速开发的全栈管理系统
actionText: 快速开始 →
actionLink: /document/
features:
- title: 简单易用
  details: 大幅度降低应用层代码难度，让每一个刚开始学习 django 和vue的新手都能快速上手。这将会是你上手学习 django+vue的最佳代码。
- title: 自由拓展
  details: 系统底层代码和业务逻辑代码分层清晰，不会发生相互干扰，便于根据自己业务方向进行拓展。
- title: 标准化目录
  details: 项目目录分层清晰，项目模式结构清晰，包名语义化，让你更加容易理解目录结构，读懂代码更加方便！
- title: 功能完善
  details: 内置完整的权限架构，包括：菜单、角色、用户、字典、参数、监控、代码生成等一系列系统常规模块。
- title: 代码生成器
  details: 在线配置表信息生成对应的代码，一键生成模块，包含增删改查/排序/导出/权限控制等操作，编译即可使用。
- title: 完全响应式布局
  details: 提供多终端适配：电脑、平板、手机等所有主流设备，提供多种不同风格的皮肤。页面美观，高端大气上档次。
footer: MIT Licensed | Copyright © 2021-2021 django-vue-admin All Rights Reserved
---
